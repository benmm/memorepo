CREATE TRIGGER mytrigger BEFORE INSERT ON memo 
FOR EACH ROW SET new.deleted_at =new.deleted_at+ INTERVAL new.delai MINUTE;

CREATE EVENT name_of_event
ON SCHEDULE EVERY 1 MINUTE
STARTS "2022-02-16 15:31:31"
DO
update memo set delai_start = delai_start + 1;

CREATE EVENT name_of_event1
ON SCHEDULE EVERY 1 MINUTE
STARTS NOW()
DO
update memo set active=0 where id in (select id from memo where delai_start >= delai)

CREATE EVENT mark_insert
ON SCHEDULE EVERY 1 MINUTE 
do update memo set delai_start = delai_start + 1;